# The main file to execute in project

# python detect_blinks.py --shape-predictor shape_predictor_68_face_landmarks.dat --video blink_detection_demo.mp4
# python detect_blinks.py --shape-predictor shape_predictor_68_face_landmarks.dat

# import the necessary packages
from scipy.spatial import distance as dist
from imutils import face_utils
import numpy as np
import imutils
import dlib
import cv2
from keras.preprocessing import image
from keras.models import load_model

global model
import SqlDatabase as db
import os

dirname = os.path.dirname(__file__)
modelPath=os.path.join(dirname, "./source/model/fine_Tune_model/finetune_model.h5")
model=load_model(modelPath)



#Details for SQL databank connection
host='localhost'
user='root'
password='root'
databaseName='Attendance_db'
table1='date'
table='employee'

databaseObject=db.sqlDatabase(host,user,password,databaseName)
databaseObject.createTable()

#Dictionary for the employee require to print name and write to SQL Server
class_names={'akash':0,'ankit':1,'ingo':2,'jeevan':3,'madeleine':4,'meena':5}
decode_class_names={v:k for k,v in class_names.items()}

#Importing the model
print('Loading Model')
model=load_model(modelPath)
print('Loaded Model')

#Function:calc_eye_aspect_ratio
#it calculates the Eye aspect Ratio  for Eye closed or Eye open
def calc_eye_aspect_ratio(eye):
	
	#compute the euclidean distances between the two sets of
	#vertical eye landmarks (x, y)-coordinates
	
	a = dist.euclidean(eye[1], eye[5])
	b = dist.euclidean(eye[2], eye[4])

	#compute the euclidean distance between the horizontal
	#eye landmark (x, y)-coordinates 
	c = dist.euclidean(eye[0], eye[3])

	# compute the eye aspect ratio
	ear = (a + b) / (2.0 * c)

	# return the eye aspect ratio
	return ear
 
#constant eye aspect ratio to indicate blink and then a second 

EYE_AR_THRESH = 0.3

#constant for the number of consecutive
#frames the eye must be below the threshold 

EYE_AR_CONSEC_FRAMES =3

print("[INFO] loading facial landmark predictor...")

def faceRecognition():

# initialize the frame counters for two cameras 

    COUNTER=0
    COUNTER1=0
    
#calculate the total number of blinks
    TotalBlink=0
    TotalBlink1=0
    frameCount_0=0
    frameCount_1=0

    detector = dlib.get_frontal_face_detector()
    #Capture Video 
    cap=cv2.VideoCapture(0)
    cap1=cv2.VideoCapture(1)
    
    # grab the indexes of the facial landmarks for the left and
    # right eye, respectively
    (leftStart, leftEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
    (rightStart, rightEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]
    predictor = dlib.shape_predictor('face_landmarks/shape_predictor_68_face_landmarks.dat')


    print('camera 0 opened',cap.isOpened())

    print('camera 1 opened',cap1.isOpened())
    
    while cap.isOpened()and cap1.isOpened() :

 
        ret,frame=cap.read()
 
        ret1,frame1=cap1.read()
        
        frame = imutils.resize(frame, height=300,width=300)
        frame1 = imutils.resize(frame1, height=300,width=300)
        
        rects = detector(frame, 0)
        for rectangle in rects:
            (x, y, w, h) = face_utils.rect_to_bb(rectangle)
            shape = predictor(frame, rectangle)
            shape = face_utils.shape_to_np(shape)
            leftEye = shape[leftStart:leftEnd]
            rightEye = shape[rightStart:rightEnd]
            leftEAR = calc_eye_aspect_ratio(leftEye)
            rightEAR = calc_eye_aspect_ratio(rightEye)
            # average the eye aspect ratio together for both eyes
            ear = (leftEAR + rightEAR) / 2.0
            #check to see if the eye aspect ratio is below the blink
    		#threshold, and if so, increment the blink frame counter
            if ear < EYE_AR_THRESH:
                COUNTER += 1
           # otherwise, the eye aspect ratio is not below the blink
            #threshold 
                     
            else:
    			# if the eyes were closed for a sufficient number of frames
                #increment the total number of blinks 
                if COUNTER >= EYE_AR_CONSEC_FRAMES:
                    TotalBlink += 1
    
    			# reset the eye frame counter
                COUNTER = 0
            #draw the total number of blinks on the frame along with
    		#the computed eye aspect ratio for the frame
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(frame, "Blinks: {}".format(TotalBlink), (15, 15),
    			cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

            
            if  TotalBlink>1:
                detected_face = frame[int(y):int(y+h), int(x):int(x+w)]
                detected_face = cv2.resize(detected_face, (224, 224))

                img_pixels = image.img_to_array(detected_face)
                img_pixels = np.expand_dims(img_pixels, axis = 0)
                img_pixels /= 255

                y_prob=model.predict([img_pixels])
                idx=decode_class_names[np.argmax(y_prob[0,:])]

                databaseObject.updateTime_in(idx)

                frameCount_0+=1

                if frameCount_0==3:
                    TotalBlink=0
                    frameCount_0=0
                #Show Person name
                cv2.putText(frame, "Face #{}".format(idx), (x - 10, y - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                
                 	# show the frame
        cv2.imshow("Frame 0", frame)

        
        
#------------------------------------------------------------------------------------#        
        rects1 = detector(frame1, 0)
        for rectangle in rects1:
            (x, y, w, h) = face_utils.rect_to_bb(rectangle)
            shape = predictor(frame1, rectangle)
            shape = face_utils.shape_to_np(shape)
            leftEye = shape[leftStart:leftEnd]
            rightEye = shape[rightStart:rightEnd]
            leftEAR = calc_eye_aspect_ratio(leftEye)
            rightEAR = calc_eye_aspect_ratio(rightEye)
            # average the eye aspect ratio together for both eyes
            ear = (leftEAR + rightEAR) / 2.0
            #check to see if the eye aspect ratio is below the blink
    		#threshold, and if so, increment the blink frame counter
            if ear < EYE_AR_THRESH:
                COUNTER1 += 1
    
    		#otherwise, the eye aspect ratio is not below the blink
    		#threshold
            else:
            #if eyes were closed for a sufficient number of
    		#then increment the total number of blinks
                if COUNTER1 >= EYE_AR_CONSEC_FRAMES:
                    TotalBlink1 += 1
    
    			# reset the eye frame counter
                COUNTER1 = 0
            #draw the total number of blinks on the frame along with
    		#the computed eye aspect ratio for the frame
            cv2.rectangle(frame1, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(frame1, "Blinks: {}".format(TotalBlink1), (15, 15),
    			cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

            if  TotalBlink1>1:
                detected_face = frame1[int(y):int(y+h), int(x):int(x+w)]
                detected_face = cv2.resize(detected_face, (224, 224))
                img_pixels = image.img_to_array(detected_face)
                img_pixels = np.expand_dims(img_pixels, axis = 0)
                img_pixels /= 255
                y_prob=model.predict([img_pixels])
                idx=decode_class_names[np.argmax(y_prob[0,:])]
                databaseObject.updateTime_out(idx)
                databaseObject.copy_Name_Monitoring_table()
                 
                frameCount_1+=1
                if frameCount_1==3:
                    TotalBlink1=0
                    frameCount_1=0
               #Show Person name
                cv2.putText(frame1, "Face #{}".format(idx), (x - 10, y - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            	#show the frame 
        cv2.imshow("Frame 1", frame1)


        key = cv2.waitKey(1) & 0xFF
    	# if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break
        
    
    # do a bit of cleanup
    cv2.destroyAllWindows()
    
 
#Call the function face Recognition 
faceRecognition()

 
