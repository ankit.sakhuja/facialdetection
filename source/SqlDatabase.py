# -*- coding: utf-8 -*-

import mysql.connector
from mysql.connector import errors
from datetime import datetime


'''

This Class is responsible for SQL database Creation
and insert , delete, upadate functions.

'''

class sqlDatabase:

    def __init__(self,host,user,password,databaseName):
        try:
        
            self.conn=mysql.connector.connect(
                            host=host,
                            user=user,
                            password=password,
                            database=databaseName)
            self.rev=0

        except errors as e:
            print (e)
            
        if self.conn.is_connected:
            print("Connection Established to database" ,databaseName)
            
        else:
            print(errors)

    def cursor(self):
        return self.conn.cursor()
    
    def commit(self):
        return self.conn.commit()
#function : Update the Server time-in done with Camera 1         
    def updateTime_in(self,name):
        self.name=str(name)
        #sql1='''select name from employee where name=%(name)s''',{'name':name}
        rev=0
        val=(name)
        mycursor=self.cursor()
        mycursor.execute('''select Name from employee where Name=%(Name)s''',{'Name':self.name})
        myresult=mycursor.fetchall()
        for x in myresult:
            if x[0]==self.name:
                rev+=1
                
        time=datetime.now()
        timeIn=datetime.strftime(time,'%H:%M:%m')
        date=datetime.strftime(time,'%Y-%m-%d')
        timeOut=''
       
        try:
            sql='INSERT INTO employee (Name,time_in, time_out,Date,rev) VALUES (%s,%s,%s,%s,%s)'
            val=(self.name,timeIn,timeOut,date,rev)
            mycursor=self.cursor()
            mycursor.execute(sql,val)
            self.commit()
            self.copyresults_time_in()

        except errors as e:
            print(e)
            
#function : Update time out
#Update the Server time-out done with Camera 2     
                  
    def updateTime_out(self,name):
        self.name=str(name)
        try:
            sql="select time_out,Name from employee where time_out='00:00:00'"
            mycursor=self.cursor()
            mycursor.execute(sql)
            myresult=mycursor.fetchall()
          
            for x in myresult:
                if x[1]==self.name:
   
                    try:
                        time=datetime.now()
                        timeOut=datetime.strftime(time,'%H:%M:%m')
                        sql="UPDATE employee SET time_out=%s WHERE Name = %s and time_out='00:00:00'"
                        val=(timeOut,str(self.name),)
                
                        myCursor=self.cursor()
                        myCursor.execute(sql,val)
                        self.commit()
                        self.copyresults_time_out()

                    except errors as e:
                        print(e)
                       
            
        except errors as e:
            print(e)

# Function: Copyresults time-in
 
 #This function will copy the time- in results from employee table to time_in table
 
          
    def copyresults_time_in(self):
        sql="INSERT IGNORE INTO time_in_table (Name,time_in,date,u_id) SELECT employee.Name, employee.time_in,employee.date,employee.u_id FROM employee inner join (SELECT  name ,max(rev) AS maxrev FROM employee  group by name)grouptt on employee.name=grouptt.name and employee.rev=grouptt.maxrev"
        mycursor=self.cursor()
        mycursor.execute(sql)
        self.commit()
       
 
# Function: Copyresults time-out
 
# This function will copy the time-out results from employee table to time_out table
 
             
    def copyresults_time_out(self):
        sql1="UPDATE time_out_table SET time_out_table.time_out=(SELECT employee.time_out FROM employee WHERE time_out_table.u_id=employee.u_id )"
        sql="INSERT IGNORE INTO time_out_table (Name,time_out,date,u_id) SELECT employee.name,employee.time_out,employee.date, employee.u_id FROM employee inner join (SELECT  name ,max(rev) AS maxrev FROM employee  group by name)grouptt on employee.name=grouptt.name and employee.rev=grouptt.maxrev"
        mycursor=self.cursor()
        mycursor.execute(sql)
        self.commit()
        
        mycursor=self.cursor()
        mycursor.execute(sql1)
        self.commit()
        
 
 #Function: Copy_Name_Monioring_table
 
 #This function will copy the Name, time-in and time-out from employee, time-in table, time-out table respectively
 
         
    def copy_Name_Monitoring_table(self):
        
        sql="INSERT IGNORE INTO monitoring_table (Name,date,u_id) SELECT employee.Name,employee.date,employee.u_id FROM employee inner join (SELECT  name ,max(rev) AS maxrev FROM employee  group by name)grouptt on employee.name=grouptt.name and employee.rev=grouptt.maxrev"
        mycursor=self.cursor()
        mycursor.execute(sql)
        self.commit()
        
        sql="UPDATE IGNORE monitoring_table SET monitoring_table.time_in=(SELECT time_in_table.time_in FROM time_in_table WHERE time_in_table.u_id=monitoring_table.u_id)"
        mycursor=self.cursor()
        mycursor.execute(sql)
        self.commit()
        
      
        sql="UPDATE IGNORE monitoring_table SET monitoring_table.time_out=(SELECT time_out_table.time_out FROM time_out_table WHERE time_out_table.u_id=monitoring_table.u_id)"
        mycursor=self.cursor()
        mycursor.execute(sql)
        self.commit()

#Function: CreateTable
#The function will create table time_out_table,employee,time_in_table, monitoring_table if not exist
 
        
    def createTable(self):
        if self.conn is not None:
            mycursor=self.cursor()
           
            mycursor.execute('CREATE TABLE IF NOT EXISTS time_out_table (Name VARCHAR(50) NOT NULL,time_out TIME NOT NULL,time_in TIME NOT NULL,date DATE,u_id int(11) UNIQUE KEY NOT NULL)')
            self.commit()
            mycursor.execute('CREATE TABLE IF NOT EXISTS employee (Name VARCHAR(50) NOT NULL,time_in TIME NOT NULL,time_out TIME NOT NULL, date DATE, rev INT(11) NOT NULL, u_id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT UNIQUE KEY)')
            self.commit()
            mycursor.execute('CREATE TABLE IF NOT EXISTS time_in_table (Name VARCHAR(50) NOT NULL,time_in TIME NOT NULL,time_out TIME NOT NULL,date DATE,u_id int(11) UNIQUE KEY NOT NULL)')
            self.commit()
            mycursor.execute('CREATE TABLE IF NOT EXISTS monitoring_table (Name VARCHAR(50) NOT NULL,time_in TIME NOT NULL,time_out TIME NOT NULL ,date DATE,u_id int(11) UNIQUE KEY NOT NULL )')
            self.commit()
          
        else:
            print('Failed to create connections')
    
