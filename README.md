# Real-Time Attendance Monitoring system using Deep Learning on Axiomtek Embedded Edge AI Computer

## **Requirements**

**Hardware**
* NVIDIA Jetson TX2, 
* e-CAM130_TRICUTX2

**Software**


* NVIDIA Jetson Jetpack V3.2 

> Tested with TesnsorFlow: v1.11.0
> Keras: 2.2.4
> OpenCV: 4.0.0-pre



**Task to Performed**

The work deals with creating an authentic face recognition system. The work focusses on the following objective:

*  To perform face detection we are using Dlib library and add eye blink detection as a security feature to make face more genuine.

*  To develop a face recognition using Convolution Neural Network (CNN) using Keras with TensorFlow backed in Python and train the model on NVIDIA Jetson TX2.
 
*  To compare the results on performance of prediction of a face based on our model and a model inspired from transfer learning concept in deep learning.
 
*  To develop a database using MySQL and creating a web interface using PHP.
 
*  To create event registering system and updating the time of a particular person.




<p>
    <img src="designflow/design%20flow.jpg" alt>
</p>
<p>
    <em>Design Flow</em>
</p>




